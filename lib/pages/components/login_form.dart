import 'package:flutter/material.dart';
import 'package:flutter_speedapp/components/rounded_button.dart';
import 'package:flutter_speedapp/components/rounded_input.dart';
import 'package:flutter_speedapp/components/rounded_password_input.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({
    Key key,
    @required this.isLogin,
    @required this.animationDuration,
    @required this.size,
    @required this.defaultLoginSize,
  }) : super(key: key);

  final bool isLogin;
  final Duration animationDuration;
  final Size size;
  final double defaultLoginSize;

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: isLogin ? 1.0 : 0.0,
      duration: animationDuration * 4,
      child: Align(
        alignment: Alignment.center,
        child: SingleChildScrollView(
          child: Container(
            width: size.width,
            height: defaultLoginSize,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  child: Image.asset(
                    'assets/images/logo_splash.png',
                    height: 200.0,
                    width: 200.0,
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                RoundedInput(
                  icon: Icons.email,
                  hint: 'Username',
                ),
                RoudedPasswordInput(hint: 'Password'),
                RoundedButton(title: 'LOGIN'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
