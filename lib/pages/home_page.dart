import 'package:flutter/material.dart';
import 'package:flutter_speedapp/pages/Elearning_page.dart';
import 'package:flutter_speedapp/pages/dashboard_page.dart';
import 'package:flutter_speedapp/pages/observations_page.dart';
import 'package:flutter_speedapp/pages/profile_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primarySwatch: Colors.brown),
        home: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/app_splash.jpg'),
                fit: BoxFit.cover),
          ),
          child: Scaffold(
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                toolbarHeight: 0.0,
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: Colors.transparent,
                    height: 150,
                    child: Flexible(
                      child: Image.asset(
                        'assets/images/logo_splash1.png',
                        width: 150.0,
                        height: 150.0,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.transparent,
                      height: 150,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Profilepage()));
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(0.0),
                                  color: Colors.grey[400],
                                  width: 150.0,
                                  height: 150.0,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Flexible(
                                        child: Image.asset(
                                          'assets/images/ico_2.png',
                                          height: 100.0,
                                          width: 100.0,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Text(
                                        'Perfil',
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.grey[700]),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ElearningPage()));
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(0.0),
                                  color: Colors.grey[400],
                                  width: 150.0,
                                  height: 150.0,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Flexible(
                                        child: Image.asset(
                                          'assets/images/ico_11.png',
                                          height: 100.0,
                                          width: 100.0,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Text(
                                        'ELearning',
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.grey[700]),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.all(0.0),
                                color: Colors.grey[400],
                                width: 150.0,
                                height: 150.0,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Flexible(
                                      child: Image.asset(
                                        'assets/images/ico_-1.png',
                                        height: 100.0,
                                        width: 100.0,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Text(
                                      'LogOut',
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.grey[700]),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ObservationPage()));
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(0.0),
                                  color: Colors.grey[400],
                                  width: 150.0,
                                  height: 150.0,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Flexible(
                                        child: Image.asset(
                                          'assets/images/ico_10.png',
                                          height: 100.0,
                                          width: 100.0,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              DashboardPage()));
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(0.0),
                                  color: Colors.grey[400],
                                  width: 150.0,
                                  height: 150.0,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Flexible(
                                        child: Image.asset(
                                          'assets/images/ico_14.png',
                                          height: 100.0,
                                          width: 100.0,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Text(
                                        'Dashboard',
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.grey[700]),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.all(0.0),
                                color: Colors.grey[400],
                                width: 150.0,
                                height: 150.0,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [],
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              )),
        ));
  }
}
