import 'package:flutter/material.dart';
import 'package:flutter_speedapp/widgets/DropdownCity.dart';
import 'package:flutter_speedapp/widgets/DropdownDep.dart';
import 'package:flutter_speedapp/widgets/DropdownForm.dart';
import 'package:flutter_speedapp/widgets/DropdownMonths.dart';
import 'package:flutter_speedapp/widgets/tab.dart';

class DashboardPage extends StatefulWidget {
  DashboardPage({Key key}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Dashboard",
      debugShowCheckedModeBanner: false,
      home: Container(
        decoration: BoxDecoration(color: Colors.white),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Column(
            children: [
              Container(
                padding: EdgeInsets.only(
                  top: 15.0,
                ),
                height: 200,
                width: double.infinity,
                color: Colors.grey[600],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        left: 15.0,
                      ),
                      height: 70.0,
                      width: double.infinity,
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(
                                left: 15.0,
                              ),
                              width: 120.0,
                              child: DropdownMonths(),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              height: 80.0,
                              width: 200.0,
                              child: DropdownCity(),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        left: 10.0,
                      ),
                      height: 70.0,
                      width: double.infinity,
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(
                                left: 15.0,
                              ),
                              width: 180.0,
                              child: DropdownDep(),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              width: 150.0,
                              child: DropdownForm(),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        top: 5.0,
                      ),
                      height: 40.0,
                      width: 300.0,
                      child: MaterialButton(
                        minWidth: 100.0,
                        height: 70.0,
                        onPressed: () {},
                        color: Colors.deepOrangeAccent[400],
                        child: Text(
                          'SEARCH',
                          style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                    height: 600.0,
                    width: double.infinity,
                    color: Colors.orange[50],
                    child: TabNavigator()),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
