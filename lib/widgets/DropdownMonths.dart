import 'package:flutter/material.dart';

class DropdownMonths extends StatefulWidget {
  @override
  _DropdownMonthsState createState() => _DropdownMonthsState();
}

class _DropdownMonthsState extends State<DropdownMonths> {
  String _value;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: DropdownButton<String>(
        dropdownColor: Colors.black,
        style: TextStyle(color: Colors.white),
        items: [
          DropdownMenuItem(
            child: Text('Enero'),
            value: 'Ene',
          ),
          DropdownMenuItem(
            child: Text('Febrero'),
            value: 'Feb',
          ),
          DropdownMenuItem(
            child: Text('Marzo'),
            value: 'Mar',
          ),
          DropdownMenuItem(
            child: Text('Abril'),
            value: 'Abri',
          ),
          DropdownMenuItem(
            child: Text('Mayo'),
            value: 'May',
          ),
          DropdownMenuItem(
            child: Text('Junio'),
            value: 'Jun',
          ),
          DropdownMenuItem(
            child: Text('Julio'),
            value: 'Jul',
          ),
          DropdownMenuItem(
            child: Text('Agosto'),
            value: 'Agos',
          ),
          DropdownMenuItem(
            child: Text('Septiembre'),
            value: 'Sep',
          ),
          DropdownMenuItem(
            child: Text('Octubre'),
            value: 'Oct',
          ),
          DropdownMenuItem(
            child: Text('Noviembre'),
            value: 'Nov',
          ),
          DropdownMenuItem(
            child: Text('Diciembre'),
            value: 'Dic',
          ),
        ],
        onChanged: (String value) {
          setState(() {
            _value = value;
          });
        },
        hint: Text(
          'Enero',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        value: _value,
      ),
    );
  }
}
