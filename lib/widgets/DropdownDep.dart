import 'package:flutter/material.dart';

class DropdownDep extends StatefulWidget {
  @override
  _DropdownDepState createState() => _DropdownDepState();
}

class _DropdownDepState extends State<DropdownDep> {
  String _value;
  @override
  Widget build(BuildContext context) {
    return Center(
        child: DropdownButton<String>(
      dropdownColor: Colors.black,
      style: TextStyle(color: Colors.white),
      items: [
        DropdownMenuItem(
          child: Text('Selecciona todos'),
          value: 'Ene',
        ),
        DropdownMenuItem(
          child: Text('Mantenimiento Aoto70'),
          value: 'Aot',
        ),
        DropdownMenuItem(
          child: Text('ManttoQro 70'),
          value: 'Quer',
        ),
        DropdownMenuItem(
          child: Text('IngenieríaAtoto70'),
          value: 'Ato',
        ),
        DropdownMenuItem(
          child: Text('COMPRASATOTO70'),
          value: 'Sji',
        ),
        DropdownMenuItem(
          child: Text('ComprasQro70'),
          value: 'Sji',
        ),
        DropdownMenuItem(
          child: Text('ComprasSJI70'),
          value: 'Sji',
        ),
      ],
      onChanged: (String value) {
        setState(() {
          _value = value;
        });
      },
      hint: Text(
        'Selecciona todos',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      value: _value,
    ));
  }
}
