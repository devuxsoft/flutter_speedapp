import 'package:flutter/material.dart';

const List<Tab> tabs = <Tab>[
  Tab(text: 'DEP'),
  Tab(text: 'EMP'),
  Tab(text: 'SEB'),
  Tab(text: 'TBS'),
  Tab(text: 'TOP'),
];

class TabNavigator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 50.0,
          backgroundColor: Colors.white,
          bottom: const TabBar(
            labelColor: Colors.black,
            indicatorColor: Colors.green,
            tabs: <Widget>[
              Tab(
                text: "DEP",
              ),
              Tab(
                text: "EMP",
              ),
              Tab(
                text: "SEB",
              ),
              Tab(
                text: "TBS",
              ),
              Tab(
                text: "TOP",
              ),
            ],
          ),
        ),
        body: const TabBarView(
          children: <Widget>[
            Center(
              child: Text("No chart data available"),
            ),
            Center(
              child: Text("No chart data available"),
            ),
            Center(
              child: Text("No chart data available"),
            ),
            Center(
              child: Text("No chart data available"),
            ),
            Center(
              child: Text("No chart data available"),
            ),
          ],
        ),
      ),
    );
  }
}



// class TabBar extends StatefulWidget {
//   @override
//   _TabBarState createState() => _TabBarState();
// }

// class _TabBarState extends State<TabBar> {
//   @override
//   Widget build(BuildContext context) {
//     return DefaultTabController(
//         initialIndex: 1,
//         length: tabs.length,
//         child: Builder(builder: (BuildContext context) {
//           final TabController tabController = DefaultTabController.of(context)!;
//           tabController.addListener(() {
//             if(!tabController.indexIsChanging){

//             }
//           });
//           return Scaffold(
//             appBar: AppBar(
//               bottom: TabBar(
//                 tabs: tabs,
//               ),
//             ),
//             body: TabBarView(
//                 children: tabs.map((Tab tab){
//                   return Center(
//                     child: Text(tab.text! + ' Tab',
//                     style: Theme.of(context).textTheme.headline5,
//                     ),
//                   );
//                 }).toList(),
//             )
//           );
//         }
//         ),
//         );
//   }
// }
