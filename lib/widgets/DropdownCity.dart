import 'package:flutter/material.dart';

class DropdownCity extends StatefulWidget {
  @override
  _DropdownCityState createState() => _DropdownCityState();
}

class _DropdownCityState extends State<DropdownCity> {
  String _value;
  @override
  Widget build(BuildContext context) {
    return Center(
        child: DropdownButton<String>(
      dropdownColor: Colors.black,
      style: TextStyle(color: Colors.white),
      items: [
        DropdownMenuItem(
          child: Text('Selecciona todos'),
          value: 'Ene',
        ),
        DropdownMenuItem(
          child: Text('Ocotlan70'),
          value: 'Oco',
        ),
        DropdownMenuItem(
          child: Text('Queretaro70'),
          value: 'Quer',
        ),
        DropdownMenuItem(
          child: Text('Atoto70'),
          value: 'Ato',
        ),
        DropdownMenuItem(
          child: Text('SJI70'),
          value: 'Sji',
        ),
      ],
      onChanged: (String value) {
        setState(() {
          _value = value;
        });
      },
      hint: Text(
        'Selecciona todos',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      value: _value,
    ));
  }
}
