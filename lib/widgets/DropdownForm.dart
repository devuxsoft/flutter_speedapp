import 'package:flutter/material.dart';

class DropdownForm extends StatefulWidget {
  @override
  _DropdownFormState createState() => _DropdownFormState();
}

class _DropdownFormState extends State<DropdownForm> {
  String _value;
  @override
  Widget build(BuildContext context) {
    return Center(
        child: DropdownButton<String>(
      dropdownColor: Colors.black,
      style: TextStyle(color: Colors.white),
      items: [
        DropdownMenuItem(
          child: Text('Selecciona todos'),
          value: 'Ene',
        ),
        DropdownMenuItem(
          child: Text('BBS70'),
          value: 'bbs',
        ),
      ],
      onChanged: (String value) {
        setState(() {
          _value = value;
        });
      },
      hint: Text(
        'Selecciona todos',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      value: _value,
    ));
  }
}
