import 'package:flutter/material.dart';
import 'package:flutter_speedapp/components/input_container.dart';

class RoundedInput extends StatelessWidget {
  const RoundedInput({Key key, @required this.icon, @required this.hint})
      : super(key: key);

  final IconData icon;
  final String hint;
  @override
  Widget build(BuildContext context) {
    return InputContainer(
      child: TextField(
        cursorColor: Colors.grey[600],
        decoration: InputDecoration(
            icon: Icon(
              icon,
              color: Colors.blue[900],
            ),
            hintText: hint,
            border: InputBorder.none),
      ),
    );
  }
}
