import 'package:flutter/material.dart';
import 'package:flutter_speedapp/components/input_container.dart';

class RoudedPasswordInput extends StatefulWidget {
  RoudedPasswordInput({Key key, @required this.hint}) : super(key: key);

  final String hint;
  @override
  _RoudedPasswordInputState createState() => _RoudedPasswordInputState();
}

class _RoudedPasswordInputState extends State<RoudedPasswordInput> {
  bool _isHidden = true;

  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  @override
  Widget build(BuildContext context) {
    return InputContainer(
      child: TextField(
        cursorColor: Colors.grey[600],
        decoration: InputDecoration(
          icon: Icon(
            Icons.lock,
            color: Colors.blue[900],
          ),
          hintText: 'Password',
          border: InputBorder.none,
          suffixIcon: IconButton(
            onPressed: _toggleVisibility,
            icon:
                _isHidden ? Icon(Icons.visibility_off) : Icon(Icons.visibility),
          ),
        ),
        obscureText: _isHidden,
        onChanged: (value) {},
      ),
    );
  }
}
